import { User } from "../domain/user";
import { Case } from "@/core/domain/case";
import { Transaction } from "@/core/domain/transaction";

export interface UserStorageService {
  user?: User;

  updateUser(user: User): void;
}

export interface TransactionsStorageService {
  transactions?: Transaction[];

  addTransaction(transaction: Transaction): void;
}

export interface AuthenticationService {
  auth(steamId: SteamId): Promise<{ user: User }>;
}

export interface PaymentService {
  purchaseCase(item: Case): Promise<Transaction>;
}

export enum ApplicationMessages {
  AUTHORIZATION_REQUIRED = "Authorization required",
  NOT_ENOUGH_BALANCE = "Not enough balance",
  PURCHASE_FAIL = "Purchase failed"
}
