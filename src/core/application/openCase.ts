import { transactionsStorageAdapter, userStorageAdapter } from "@/core/adapters/storage.adapter";
import { Case } from "@/core/domain/case";
import { paymentAdapter } from "@/core/adapters/payment.adapter";
import {
  ApplicationMessages,
  PaymentService,
  TransactionsStorageService,
  UserStorageService
} from "@/core/application/interfaces";
import { enoughBalance } from "@/core/domain/user";


export function OpenCase() {
  const userStorageService: UserStorageService = userStorageAdapter();
  const paymentService: PaymentService = paymentAdapter();
  const transactionsStorageService: TransactionsStorageService = transactionsStorageAdapter();

  async function purchase(item: Case) {
    if (!userStorageService.user) {
      throw new Error(ApplicationMessages.AUTHORIZATION_REQUIRED);
    }

    if (!enoughBalance(userStorageService.user, item)) {
      throw new Error(ApplicationMessages.NOT_ENOUGH_BALANCE);
    }

    const transaction = await paymentService.purchaseCase(item);

    if (!transaction) {
      throw new Error(ApplicationMessages.PURCHASE_FAIL);
    }

    transactionsStorageService.addTransaction(transaction)

    return transaction;
  }

  return {
    purchase
  };
}
