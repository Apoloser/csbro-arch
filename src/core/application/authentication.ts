import { AuthenticationService, UserStorageService } from "@/core/application/interfaces";
import { authAdapter } from "@/core/adapters/auth.adapter";
import { userStorageAdapter } from "@/core/adapters/storage.adapter";

export function Authentication() {
  const authAdapterService: AuthenticationService = authAdapter();
  const userStorageService: UserStorageService = userStorageAdapter();

  async function authenticate(name: string): Promise<void> {
    const { user } = await authAdapterService.auth(name);
    userStorageService.updateUser(user);
  }

  return {
    user: userStorageService.user,
    authenticate
  }
}
