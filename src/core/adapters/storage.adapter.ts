import { TransactionsStorageService, UserStorageService } from "@/core/application/interfaces";
import { reactive, readonly } from "vue";
import { User } from "@/core/domain/user";
import { Transaction } from "@/core/domain/transaction";

const userState = reactive({ user: undefined }) as any;
const transactionState = reactive({ transactions: [] }) as any;

export function userStorageAdapter(): UserStorageService {
  const updateUser = (user: User) => {
    userState.user = user;
  };

  return { updateUser, user: readonly(userState.user) };
}

export function transactionsStorageAdapter(): TransactionsStorageService {
  const addTransaction = (transaction: Transaction) => {
    transactionState.transactions.unshift(transaction);
  };

  return { addTransaction, transactions: readonly(transactionState.transactions) };
}
