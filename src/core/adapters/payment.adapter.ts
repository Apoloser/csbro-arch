import { random } from "lodash";
import { PaymentService } from "../application/interfaces";
import { apiCall } from "../services/api.service";
import { Case } from "@/core/domain/case";
import { nanoid } from "nanoid";
import { Transaction } from "@/core/domain/transaction";

export function paymentAdapter(): PaymentService {
  return {
    async purchaseCase(chest: Case): Promise<Transaction> {
      const wonItem = chest.items[random(0, chest.items.length - 1)];
      return apiCall({ id: nanoid(), wonItem, chest, date: new Date().toISOString() });
    }
  };
}
