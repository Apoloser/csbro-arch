import { nanoid } from "nanoid";
import { AuthenticationService } from "../application/interfaces";
import { apiCall } from "../services/api.service";

export function authAdapter(): AuthenticationService {
  return {
    async auth(name: string) {
      const user = await apiCall({
        id: "test-id",
        name,
        balance: 0,
      });

      return { user, session: { refreshToken: nanoid(), token: nanoid() } };
    },
  };
}
