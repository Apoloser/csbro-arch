export type Item = {
  id: UniqueId;
  name: string;
  icon: string;
};
