import { Case } from "@/core/domain/case";

export type User = {
  id: UniqueId;
  name: string;
  balance: number;
};

export function enoughBalance(user: User, chest: Case): boolean {
  return user.balance >= chest.price;
}
