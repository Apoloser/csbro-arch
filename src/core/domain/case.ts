import { Item } from "./item";

export type Case = {
  id: UniqueId;
  name: string;
  items: Item[];
  price: number;
  image: string;
};
