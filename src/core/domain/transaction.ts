import { Case } from "@/core/domain/case";
import { Item } from "@/core/domain/item";

export type Transaction = {
  id: UniqueId;
  chest: Case;
  wonItem: Item;
  date: DateTimeString;
};
