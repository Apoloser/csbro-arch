type SteamId = string;
type UniqueId = string;
type DateTimeString = string;

type Price = {
  value: number;
  currency: string;
};
